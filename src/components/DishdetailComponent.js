import React, {Component} from 'react';
import {Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalBody, ModalHeader, Row, Label} from 'reactstrap';
import { Link } from 'react-router-dom';
import {Control, LocalForm, Errors} from 'react-redux-form';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len)

class CommentForm extends Component {
  constructor(props){
    super(props);
    this.state={
      isModalOpen:false
    }
    this.toggleModal=this.toggleModal.bind(this);
    this.handleComment=this.handleComment.bind(this)
  }

  toggleModal() {
    this.setState({
        isModalOpen: !this.state.isModalOpen
    });
  }

  handleComment(values) {
    console.log(values);
    this.toggleModal();
    this.props.postComment(this.props.dishId, values.rating, values.yourname, values.textarea); 
  }

  render() {
    return(
      <div>
        <button onClick={this.toggleModal} type="button" class="btn btn-outline-secondary"><span class="fa fa-pencil fa-lg"></span> Submit Comment</button>        
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <div className="row row-content">
            <div className="col-12">
          <ModalHeader>
              Submit Comment
          </ModalHeader>
          </div>
          <div className="col-12 col-md-9">
          <ModalBody>
            <LocalForm onSubmit={(values)=>this.handleComment(values)}>
                <Row className="form-group">
                    <Label htmlFor='rating'>Rating</Label>
                    <Control.select model=".rating" id="rating" 
                    className="form-control"
                    name="rating"
                    >
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </Control.select>
                </Row>
                <Row className="form-group">
                    <Label htmlFor='yourname'>Your Name</Label>
                    <Control.text model=".yourname" 
                    className="form-control"
                    id="yourname" name="yourname" placeholder="Your Name"
                    validators={{
                      required, minLength: minLength(3), maxLength: maxLength(15)
                      }}  
                    />
                    <Errors
                     className="text-danger"
                     model=".yourname"
                     show="touched"
                     messages={{
                         required: 'Required',
                         minLength: 'Must be greater than 2 characters',
                         maxLength: 'Must be 15 characters or less'
                     }} 
                    />
                </Row>
                <Row className="form-group">
                    <Label htmlFor='comment'>Comment</Label>
                        <Control.textarea model=".textarea" 
                        className="form-control"
                        type="textarea" rows="6" name="comment"
                         />                   
                </Row>
                <Button onClick={this.toggleModal} type="submit" value="submit" color="primary">Submit</Button>
            </LocalForm>
          </ModalBody>
          </div>
          </div>
        </Modal>        
      </div>
      );
  }
}

      function RenderDish({dish}) {
        if (dish != null)
          return(
            <FadeTransform in 
              transformProps={{exitTransform: 'scale(0.5) translateY(-50%)'}}>
              <Card>
                <CardImg top src={baseUrl + dish.image} alt={dish.name} />
                <CardBody>
                  <CardTitle>{dish.name}</CardTitle>
                  <CardText>{dish.description}</CardText>
                  </CardBody>
              </Card>
            </FadeTransform>
          );
        else
          return(
            <div></div>
          );
      }

      function RenderComments({comment, postComment, dishId}) {       
        if(comment != null) {
          return (
            <div className="container">
              <div className="row">
                <div>
                  <h4>Comments</h4>
                </div>
                  <Stagger in>{ 
                    comment.map((big) => {            
                      return(
                        <Fade in>
                          <div key={big.id}>
                          <ul className="list-unstyled">
                            <li>{big.comment}<br />{big.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(big.date)))} </li> 
                          </ul>
                          </div>
                        </Fade>
                      );
                    })
                  }
                  </Stagger>
                <CommentForm dishId={dishId}
                postComment={postComment} />
              </div>
            </div>
          );
        }
        else{
          return(
            <div></div>
          );
        }
      }

    const DishDetail = (props) => {
      if (props.isLoading) {
        return(
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.errMess) {
        return(
            <div className="container">
                <div className="row">            
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
    else if (props.dish != null) {
        return(
          <div className="container">
            <div className="row">
              <Breadcrumb>
                <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                <BreadcrumbItem active>{props.dish.name} </BreadcrumbItem>
             </Breadcrumb>
             <div className="col-12">
              <h3>{props.dish.name}</h3>
              <hr />
             </div>
           </div>
           <div className="row">
              <div className="col-12 col-md-5 m-1">
               <RenderDish dish={props.dish} />
              </div>
              <div className="col-12 col-md-5 m-1">
                <RenderComments comment={props.comments}
                  postComment={props.postComment}
                  dishId={props.dish.id} /> 
                </div>
           </div>
         </div>
        );
      
      }
      else{
        return(
          <div></div>
        )
      }
    }
  
export default DishDetail;